<?php

$file = fopen($_FILES['fedexLog']['tmp_name'], 'r');

$csv = array_map('str_getcsv', file($_FILES['fedexLog']['tmp_name']));
$fields = $csv[0];
$columns = array();
$trackingColumns = array();
$commodityColumns = array();

// Get the columns for the database
for($i = 0, $size = count($fields); $i < $size; $i++) {
    if ($fields[$i] !== 'Tracking ID Charge Description' && $fields[$i] !== 'Tracking ID Charge Amount' && $fields[$i] !== 'Commodity Description' && $fields[$i] !== 'Commodity Country Code') {

        $column = new stdClass();
        $column->field = str_replace(' ', '_', $fields[$i]);
        $column->field = str_replace('#', '', $column->field);
        $column->field = str_replace('/', '', $column->field);
        $column->index = $i;
        $column->type = null;

        array_push($columns, $column);
    }
}

// Get the tracking columns
for($i = 0, $size = count($fields); $i < $size; $i++) {
    if ($fields[$i] === 'Tracking ID Charge Description') {

        $column = new stdClass();
        $column->key_index = $i;
        $column->value_index = $i + 1;

        array_push($trackingColumns, $column);
    }
}

// Get the commodity columns
for($i = 0, $size = count($fields); $i < $size; $i++) {
    if ($fields[$i] === 'Commodity Description') {

        $column = new stdClass();
        $column->key_index = $i;
        $column->value_index = $i + 1;

        array_push($commodityColumns, $column);
    }
}

// Determine default types
for ($i = 1, $size = count($csv); $i < $size; $i++) {
    foreach ($columns as $column) {
        if (!$column->type) {
            if (is_numeric($csv[$i][$column->index])) {
                $column->type = 'integer';
            } elseif (is_string($csv[$i][$column->index]) && trim($csv[$i][$column->index]) !== '') {
                $column->type = 'string';
            }
        }
    }
}

// Backfill unknown types and possible dates
foreach ($columns as $column) {
    if (!$column->type) {
        $column->type = 'string';
    }

    if (strpos(strtolower($column->field), 'date')) {
        $column->type = 'date';
    }
}

//print_r($columns);

// Create connection
//$conn = new mysqli('localhost', 'root', 'DuckS@uc3');

//DB details
$dbHost = 'aa1rxttvgy0hodz.cybsu4vqta9k.us-east-2.rds.amazonaws.com';
$dbUsername = 'fedeximporter';
$dbPassword = 'Password1!';
$dbName = 'ebdb';

//Create connection and select DB
$db = new mysqli($dbHost, $dbUsername, $dbPassword, $dbName);

if($db->connect_errno > 0){
    die('Unable to connect to database [' . $db->connect_error . ']');
}

if ($db->query("DESCRIBE `imported_data`")) {
	// Drop the existing import tables
	$sql = "DROP TABLE imported_data";
	
	if(!$result = $db->query($sql)) {
    	echo('<p>'.$sql.'</p>');
    	die('There was an error running the query [' . $db->error . ']');
	}
}

if ($db->query("DESCRIBE `tracking_id_charges`")) {
	// Drop the existing import tables
	$sql = "DROP TABLE tracking_id_charges";

	if(!$result = $db->query($sql)) {
    	echo('<p>'.$sql.'</p>');
    	die('There was an error running the query [' . $db->error . ']');
	}
}

if ($db->query("DESCRIBE `commodities`")) {
	// Drop the existing import tables
	$sql = "DROP TABLE commodities";

	if(!$result = $db->query($sql)) {
    	echo('<p>'.$sql.'</p>');
    	die('There was an error running the query [' . $db->error . ']');
	}
}

// Create the import tables
$sql = "CREATE TABLE imported_data (\r\n`id` INT NOT NULL AUTO_INCREMENT,\r\n";

foreach ($columns as $column) {
    switch ($column->type) {
        CASE 'string':
            $sql = $sql . "`".$column->field."` TEXT(1024) NULL,\r\n";
            break;

        CASE 'integer':
            $sql = $sql . "`".$column->field."` FLOAT NULL,\r\n";
            break;

        CASE 'date':
            $sql = $sql . "`".$column->field."` DATETIME NULL,\r\n";
            break;
    }
}

$sql = $sql . "PRIMARY KEY (`id`))";

if(!$result = $db->query($sql)) {
    echo('<p>'.$sql.'</p>');
    die('There was an error running the query [' . $db->error . ']');
}

$sql = "CREATE TABLE tracking_id_charges (\r\n";
$sql = $sql . "`imported_data_id` INT NULL,\r\n";
$sql = $sql . "`Tracking_ID_Charge_Description` TEXT(1024) NULL,\r\n";
$sql = $sql . "`Tracking_ID_Charge_Amount` FLOAT NULL\r\n";
$sql = $sql . ")";

if(!$result = $db->query($sql)) {
    echo('<p>'.$sql.'</p>');
    die('There was an error running the query [' . $db->error . ']');
}

$sql = "CREATE TABLE commodities (\r\n";
$sql = $sql . "`imported_data_id` INT NULL,\r\n";
$sql = $sql . "`Commodity_Description` TEXT(1024) NULL,\r\n";
$sql = $sql . "`Commodity_Country_Code` TEXT(1024) NULL\r\n";
$sql = $sql . ")";

if(!$result = $db->query($sql)) {
    echo('<p>'.$sql.'</p>');
    die('There was an error running the query [' . $db->error . ']');
}

// Import the data
for ($i = 1, $size = count($csv); $i < $size; $i++) {
    $sql = "INSERT INTO imported_data (";

    $a = array_map(function($obj) { return $obj->field; }, $columns);
    $a = implode(", ", $a);

    $sql = $sql . $a;
    $sql = $sql . ") VALUES (";

    $values = array();
    foreach ($columns as $column) {
        $value = $csv[$i][$column->index];
        $value = str_replace("'", "", $value);
        $value = trim($value) === "" ? "NULL" : trim($value);

        switch ($column->type) {
            CASE 'string':
                $value = $value === "NULL" ? "NULL" : "'".$value."'";
                break;

            CASE 'integer':
                $value = $value === "NULL" ? "NULL" : (double)$value;
                break;

            CASE 'date':
                $value = $value === "NULL" || $value === "0" ? "NULL" : "'".$value."'";
                break;
        }
        array_push($values, $value);
    }

    $values = implode(", ", $values);

    $sql = $sql . $values . ")";

    if(!$result = $db->query($sql)) {
        echo('<p>'.$sql.'</p>');
        die('There was an error running the query [' . $db->error . ']');
    }

    $id = $db->insert_id;

    foreach($trackingColumns as $column) {
        $key = $csv[$i][$column->key_index];
        $value = $csv[$i][$column->value_index];

        if (trim($key) !== '') {
            $sql = "INSERT INTO tracking_id_charges (imported_data_id, Tracking_ID_Charge_Description, Tracking_ID_Charge_Amount) VALUES(".$id.", '".$key."', ".$value.")";

            if(!$result = $db->query($sql)) {
                echo('<p>'.$sql.'</p>');
                die('There was an error running the query [' . $db->error . ']');
            }
        }
    }

    foreach($commodityColumns as $column) {
        $key = $csv[$i][$column->key_index];
        $value = $csv[$i][$column->value_index];

        if (trim($key) !== '') {
            $sql = "INSERT INTO commodities (imported_data_id, Commodity_Description, Commodity_Country_Code) VALUES(".$id.", '".$key."', '".$value."')";

            if(!$result = $db->query($sql)) {
                echo('<p>'.$sql.'</p>');
                die('There was an error running the query [' . $db->error . ']');
            }
        }
    }
}