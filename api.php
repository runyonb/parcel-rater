<?php

$host = 'aa1rxttvgy0hodz.cybsu4vqta9k.us-east-2.rds.amazonaws.com';
$username = 'fedeximporter';
$password = 'Password1!';
$schema = 'ebdb';

$db = new mysqli($host, $username, $password, $schema);

if ($db->connect_errno > 0) {
    die('Unable to connect to database [' . $db->connect_error . ']');
}

$sql = 'SELECT DISTINCT serviceType, groundService FROM vw_servicebreakdown;';

$result = $db->query($sql);
$db->close();

$products = array();

while ($row = mysqli_fetch_assoc($result)) {
    $product = array('serviceType' => $row['serviceType'], 'groundService' => $row['groundService'], 'categories' => array());

    $db = new mysqli($host, $username, $password, $schema);

    if ($db->connect_errno > 0) {
        die('Unable to connect to database [' . $db->connect_error . ']');
    }
    $sql = "CALL getTransportationChargeAmount('" . $row['serviceType'] . "', ";
    $sql = $sql . ($row['groundService'] == '' ? "NULL" : "'" . $row['groundService'] . "'") . ")";

    $sp_result = $db->query($sql);

    if ($sp_result) {
        $category = array('Description' => 'Transportation Charge Amount', 'TotalChargeAmount' => money_format('%i', 0), 'Zone1to5' => money_format('%i', 0), 'Zone6to10' => money_format('%i', 0), 'Zone11to16' => money_format('%i', 0), 'ZoneIsNull' => money_format('%i', 0), 'ZoneIsAlpha' => money_format('%i', 0));

        while ($r = mysqli_fetch_assoc($sp_result)) {
            $category[$r['type']] = $r['amount'];
        }

        array_push($product['categories'], $category);

        $sp_result->close();
        $db->close();
    }

    $db = new mysqli($host, $username, $password, $schema);

    if ($db->connect_errno > 0) {
        die('Unable to connect to database [' . $db->connect_error . ']');
    }
    $sql = "CALL getDiscountAmount('" . $row['serviceType'] . "', ";
    $sql = $sql . ($row['groundService'] == '' ? "NULL" : "'" . $row['groundService'] . "'") . ")";

    $sp_result = $db->query($sql);

    if ($sp_result) {
        $category = array('Description' => 'Discount', 'TotalChargeAmount' => money_format('%i', 0), 'Zone1to5' => money_format('%i', 0), 'Zone6to10' => money_format('%i', 0), 'Zone11to16' => money_format('%i', 0), 'ZoneIsNull' => money_format('%i', 0), 'ZoneIsAlpha' => money_format('%i', 0));

        while ($r = mysqli_fetch_assoc($sp_result)) {
            $category[$r['type']] = $r['amount'];
        }

        array_push($product['categories'], $category);

        $sp_result->close();
        $db->close();
    }

    $db = new mysqli($host, $username, $password, $schema);

    if ($db->connect_errno > 0) {
        die('Unable to connect to database [' . $db->connect_error . ']');
    }
    $sql = "CALL getFuelSurcharge('" . $row['serviceType'] . "', ";
    $sql = $sql . ($row['groundService'] == '' ? "NULL" : "'" . $row['groundService'] . "'") . ")";

    $sp_result = $db->query($sql);

    if ($sp_result) {
        $category = array('Description' => 'Fuel Surcharge', 'TotalChargeAmount' => money_format('%i', 0), 'Zone1to5' => money_format('%i', 0), 'Zone6to10' => money_format('%i', 0), 'Zone11to16' => money_format('%i', 0), 'ZoneIsNull' => money_format('%i', 0), 'ZoneIsAlpha' => money_format('%i', 0));

        while ($r = mysqli_fetch_assoc($sp_result)) {
            $category[$r['type']] = $r['amount'];
        }

        array_push($product['categories'], $category);

        $sp_result->close();
        $db->close();
    }

    $db = new mysqli($host, $username, $password, $schema);

    if ($db->connect_errno > 0) {
        die('Unable to connect to database [' . $db->connect_error . ']');
    }
    $sql = "CALL getNetChargeAmount('" . $row['serviceType'] . "', ";
    $sql = $sql . ($row['groundService'] == '' ? "NULL" : "'" . $row['groundService'] . "'") . ")";

    $sp_result = $db->query($sql);

    if ($sp_result) {
        $category = array('Description' => 'Net Charge Amount', 'TotalChargeAmount' => money_format('%i', 0), 'Zone1to5' => money_format('%i', 0), 'Zone6to10' => money_format('%i', 0), 'Zone11to16' => money_format('%i', 0), 'ZoneIsNull' => money_format('%i', 0), 'ZoneIsAlpha' => money_format('%i', 0));

        while ($r = mysqli_fetch_assoc($sp_result)) {
            $category[$r['type']] = $r['amount'];
        }

        array_push($product['categories'], $category);

        $sp_result->close();
        $db->close();
    }

    array_push($products, $product);
}
header('Content-Type: application/json');
echo json_encode($products);