<?php

$db = new mysqli('localhost', 'root', 'DuckS@uc3', 'fedex');

if ($db->connect_errno > 0) {
    die('Unable to connect to database [' . $db->connect_error . ']');
}

$sql = 'SELECT DISTINCT serviceType, groundService FROM vw_servicebreakdown;';

$result = $db->query($sql);
$db->close();

$products = array();

while ($row = mysqli_fetch_assoc($result)) {
    $product = array('serviceType' => $row['serviceType'], 'groundService' => $row['groundService'], 'categories' => array());

    $db = new mysqli('localhost', 'root', 'DuckS@uc3', 'fedex');

    if ($db->connect_errno > 0) {
        die('Unable to connect to database [' . $db->connect_error . ']');
    }
    $sql = "CALL getTransportationChargeAmount('" . $row['serviceType'] . "', ";
    $sql = $sql . ($row['groundService'] == '' ? "NULL" : "'" . $row['groundService'] . "'") . ")";

    $sp_result = $db->query($sql);

    if ($sp_result) {
        $category = array('Description' => 'Transportation Charge Amount', 'TotalChargeAmount' => 0, 'Zone1to5' => 0, 'Zone6to10' => 0, 'Zone11to16' => 0, 'ZoneIsNull' => 0, 'ZoneIsAlpha' => 0);

        while ($r = mysqli_fetch_assoc($sp_result)) {
            $category[$r['type']] = $r['amount'];
        }

        array_push($product['categories'], $category);

        $sp_result->close();
        $db->close();
    }

    $db = new mysqli('localhost', 'root', 'DuckS@uc3', 'fedex');

    if ($db->connect_errno > 0) {
        die('Unable to connect to database [' . $db->connect_error . ']');
    }
    $sql = "CALL getNetChargeAmount('" . $row['serviceType'] . "', ";
    $sql = $sql . ($row['groundService'] == '' ? "NULL" : "'" . $row['groundService'] . "'") . ")";

    $sp_result = $db->query($sql);

    if ($sp_result) {
        $category = array('Description' => 'Net Charge Amount', 'TotalChargeAmount' => 0, 'Zone1to5' => 0, 'Zone6to10' => 0, 'Zone11to16' => 0, 'ZoneIsNull' => 0, 'ZoneIsAlpha' => 0);

        while ($r = mysqli_fetch_assoc($sp_result)) {
            $category[$r['type']] = $r['amount'];
        }

        array_push($product['categories'], $category);

        $sp_result->close();
        $db->close();
    }

    $db = new mysqli('localhost', 'root', 'DuckS@uc3', 'fedex');

    if ($db->connect_errno > 0) {
        die('Unable to connect to database [' . $db->connect_error . ']');
    }
    $sql = "CALL getDiscountAmount('" . $row['serviceType'] . "', ";
    $sql = $sql . ($row['groundService'] == '' ? "NULL" : "'" . $row['groundService'] . "'") . ")";

    $sp_result = $db->query($sql);

    if ($sp_result) {
        $category = array('Description' => 'Discount', 'TotalChargeAmount' => 0, 'Zone1to5' => 0, 'Zone6to10' => 0, 'Zone11to16' => 0, 'ZoneIsNull' => 0, 'ZoneIsAlpha' => 0);

        while ($r = mysqli_fetch_assoc($sp_result)) {
            $category[$r['type']] = $r['amount'];
        }

        array_push($product['categories'], $category);

        $sp_result->close();
        $db->close();
    }

    $db = new mysqli('localhost', 'root', 'DuckS@uc3', 'fedex');

    if ($db->connect_errno > 0) {
        die('Unable to connect to database [' . $db->connect_error . ']');
    }
    $sql = "CALL getFuelSurcharge('" . $row['serviceType'] . "', ";
    $sql = $sql . ($row['groundService'] == '' ? "NULL" : "'" . $row['groundService'] . "'") . ")";

    $sp_result = $db->query($sql);

    if ($sp_result) {
        $category = array('Description' => 'Fuel Surcharge', 'TotalChargeAmount' => 0, 'Zone1to5' => 0, 'Zone6to10' => 0, 'Zone11to16' => 0, 'ZoneIsNull' => 0, 'ZoneIsAlpha' => 0);

        while ($r = mysqli_fetch_assoc($sp_result)) {
            $category[$r['type']] = $r['amount'];
        }

        array_push($product['categories'], $category);

        $sp_result->close();
        $db->close();
    }

    array_push($products, $product);
}
?>
<html>
<head>
    <title>Fedex</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <?php
    foreach ($products as $product) {
    ?>
    <div class="row">
        <div class="col-sm-12">
            <h1><?php echo($product['serviceType']); ?></h1>
            <h2><?php echo($product['groundService']); ?></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6"><label>Description</label></div>
        <div class="col-sm-1"><label>Total Charge Amount</label></div>
        <div class="col-sm-1"><label>Zones 1-5</label></div>
        <div class="col-sm-1"><label>Zones 6-10</label></div>
        <div class="col-sm-1"><label>Zones 11-16</label></div>
        <div class="col-sm-1"><label>No Zone</label></div>
        <div class="col-sm-1"><label>Zones A-Z</label></div>
    </div>
        <?php
        foreach ($product['categories'] as $category) {
        ?>
    <div class="row">
        <div class="col-sm-6"><?php echo($category['Description']); ?></div>
        <div class="col-sm-1"><?php echo($category['TotalChargeAmount']); ?></div>
        <div class="col-sm-1"><?php echo($category['Zone1to5']); ?></div>
        <div class="col-sm-1"><?php echo($category['Zone6to10']); ?></div>
        <div class="col-sm-1"><?php echo($category['Zone11to16']); ?></div>
        <div class="col-sm-1"><?php echo($category['ZoneIsNull']); ?></div>
        <div class="col-sm-1"><?php echo($category['ZoneIsAlpha']); ?></div>
    </div>
        <?php
        }
        ?>
    <?php
    }
    ?>
</div>
</body>
</html>
