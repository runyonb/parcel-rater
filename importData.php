<?php
//load the database configuration file
include 'dbConfig.php';

if(isset($_POST['importSubmit'])){
    
    //validate whether uploaded file is a csv file
    $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
    if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
        if(is_uploaded_file($_FILES['file']['tmp_name'])){
            
            //open uploaded csv file with read only mode
            $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
            
            //skip first line
            fgetcsv($csvFile);
            $db->query("TRUNCATE TABLE importeddata"); 
            
            //parse data from csv file line by line
            while(($line = fgetcsv($csvFile)) !== FALSE){


                if($prevResult->num_rows > 0){

                      
                   
                }else{
                    
                    //insert member data into database
                    $db->query("INSERT INTO importeddata(orgAmountDue, transportationCost, netChargeAmount, serviceType, groundServiceType, ratedWeight,numberOfPieces ) VALUES ('".$line[4]."','".$line[9]."','".$line[10]."','".$line[11]."','".$line[12]."','".$line[20]."','".$line[22]."')");
                }
            }
            
            //close opened csv file
            fclose($csvFile);

            $qstring = '?status=succ';
        }else{
            $qstring = '?status=err';
        }
    }else{
        $qstring = '?status=invalid_file';
    }
}

//redirect to the listing page
header("Location: index.php".$qstring);