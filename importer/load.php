<!DOCTYPE html>
<html lang="en">
<head>
  <title>Shipping Data Importer</title
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style type="text/css">
    .panel-heading a{float: right;}
    #importFrm{margin-bottom: 20px;display: none;}
    #importFrm input[type=file] {display: inline;}
  </style>
</head>
<body>

<div class="container">
    <h2>Import </h2>
    <?php if(!empty($statusMsg)){
        echo '<div class="alert '.$statusMsgClass.'">'.$statusMsg.'</div>';
    } ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            Data list
            <a href="javascript:void(0);" onclick="$('#importFrm').slideToggle();">Import Data</a>
        </div>
        <div class="panel-body">

<div class="col-md-4 col-md-offset-4 text-center">
      <div class="search-box">
        <div class="caption">
          <h3>Log in to account</h3>
          <p>Find to All</p>
        </div>
        <form action="" class="loginForm">
          <div class="input-group">
            <input type="text" id="name" class="form-control" placeholder="Full Name">
            <input type="password" id="paw" class="form-control" placeholder="Password">
            <input type="submit" id="submit" class="form-control" value="Submit">
          </div>
        </form>
      </div>
    </div>

            <form action="importData.php" method="post" enctype="multipart/form-data" id="importFrm">
                <input type="file" name="file" />
                <input type="submit" class="btn btn-primary" name="importSubmit" value="IMPORT">
            </form>



            <table class="table table-bordered">
                <thead>
                    <tr>
                      <th>Original Amount Due</th>
                      <th>Transportation Charge Amount</th>
                      <th>Net Charge Amount</th>
                      <th>Service Type</th>
                      <th>Ground Service</th>
                      <th>Rated Weight Amount</th>
                       <th># Pieces</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    //get rows query
                    $query = $db->query("SELECT * FROM importeddata");
                    if($query->num_rows > 0){ 
                        while($row = $query->fetch_assoc()){
                        ?>
                    <tr>
                      <td><?php echo $row['orgAmountDue']; ?></td>
                      <td><?php echo $row['transportationCost']; ?></td>
                      <td><?php echo $row['netChargeAmount']; ?></td>
                      <td><?php echo $row['serviceType']; ?></td>
                      <td><?php echo $row['groundServiceType']; ?></td>
                      <td><?php echo $row['ratedWeight']; ?></td>
                      <td><?php echo $row['numberOfPieces']; ?></td>
                    </tr>
                    <?php } }else{ ?>
                    <tr><td colspan="7">No Data found.....</td></tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

</body>
</html>