<?php
//load the database configuration file
include 'dbConfig.php';
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <title>Shipping Data Importer</title
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="style.css">
    <style type="text/css">
        .panel-heading a{float: right;}
        #importFrm{margin-bottom: 20px;display: none;}
        #importFrm input[type=file] {display: inline;}
    </style>
</head>

<section style="background:#efefe9;">
    <div class="container">
        <div class="row">
            <div class="board">

                <div class="board-inner">
                    <ul class="nav nav-tabs" id="myTab">
                        <div class="liner"></div>
                        <li class="active">
                            <a href="#home" data-toggle="tab" title="welcome">
                      <span class="round-tabs one">
                              <i class="glyphicon glyphicon-home"></i>
                      </span>
                            </a></li>

                        <li><a href="#profile" data-toggle="tab" title="profile">
                     <span class="round-tabs two">
                         <i class="glyphicon glyphicon-download-alt"></i>
                     </span>
                            </a>
                        </li>
                        <li><a href="#messages" data-toggle="tab" title="bootsnipp goodies">
                     <span class="round-tabs three">
                          <i class="glyphicon glyphicon-ok"></i>
                     </span> </a>
                        </li>

                        <li><a href="#settings" data-toggle="tab" title="blah blah">
                         <span class="round-tabs four">
                              <i class="glyphicon glyphicon-search"></i>
                         </span>
                            </a></li>

                        <li><a href="#doner" data-toggle="tab" title="completed">
                         <span class="round-tabs five">
                              <i class="glyphicon glyphicon-send"></i>
                         </span> </a>
                        </li>

                    </ul></div>

                <div class="tab-content">
                    <div class="tab-pane fade in active" id="home">

                        <h3 class="head text-center">System Login</h3>



                        <div class="row">
                            <div class="col-md-4 col-md-offset-4">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Login via site</h3>
                                    </div>
                                    <div class="panel-body">
                                        <form accept-charset="UTF-8" role="form">
                                            <fieldset>
                                                <div class="form-group">
                                                    <input class="form-control" placeholder="yourmail@example.com" name="email" type="text">
                                                </div>
                                                <div class="form-group">
                                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input name="remember" type="checkbox" value="Remember Me"> Remember Me
                                                    </label>
                                                </div>
                                                <input class="btn btn-lg btn-success btn-block" type="submit" value="Login">
                                            </fieldset>
                                        </form>
                                        <hr/>
                                        <center><h4>OR</h4></center>
                                        <input class="btn btn-lg btn-facebook btn-block" type="submit" value="Login via facebook">
                                    </div>
                                </div>
                            </div>
                        </div>





                        <!-- <p class="text-center">
                  <a href="" class="btn btn-success btn-outline-rounded green"> Login <span style="margin-left:10px;" class="glyphicon glyphicon-send"></span></a>
              </p> -->


                    </div>
                    <div class="tab-pane fade" id="profile">
                        <h3 class="head text-center">Import Data</h3>
                        <div class="container">
                            <form action="fedex-importer.php" method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-5 col-md-offset-2">
                                        <div class="form-group">
                                            <label>Select a .CSV File to import</label>
                                            <input type="file" name="fedexLog" id="fedexLog" class="form-control"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-md-offset-4">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary" name="submit">Process File</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-md-offset-4">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Login via FedEX</h3>
                                    </div>

                                    <div class="panel-body">




                                        <form accept-charset="UTF-8" role="form">
                                            <fieldset>
                                                <div class="form-group">
                                                    <input class="form-control" placeholder="yourmail@example.com" name="email" type="text">
                                                </div>
                                                <div class="form-group">
                                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                                </div>
                                                <div class="form-group">
                                                    <label for="sel1">Select Invoice Date Range:</label>
                                                    <select class="form-control" id="sel1">
                                                        <option>1 Month</option>
                                                        <option>3 Months</option>
                                                        <option>6 Month</option>
                                                        <option>1 Year</option>
                                                    </select>
                                                </div>





                                                <input class="btn btn-lg btn-success btn-block" type="submit" value="Login">





                                            </fieldset>
                                        </form>

                                        <center><h4>OR</h4></center>

                                        <div class="form-group">
                                            <label for="sel1">Select previously Saved Data</label>
                                            <select class="form-control" id="sel1">
                                                <option>company 1</option>
                                                <option>company 2</option>
                                                <option>company 3</option>
                                                <option>company 4</option>
                                            </select>
                                        </div>
                                        <input class="btn btn-lg btn-success btn-block" type="submit" value="Load">
                                    </div>
                                </div>
                            </div>
                        </div>








                        <!--      <p class="text-center">
                       <a href="" class="btn btn-success btn-outline-rounded green"> Download FEDEX Data <span style="margin-left:10px;" class="glyphicon glyphicon-send"></span></a>
                   </p> -->

                    </div>
                    <div class="tab-pane fade" id="messages">
                        <h3 class="head text-center">Review Import</h3>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Service Type</th>
                                <th>Ground Service</th>

                                <th>Transportation Charge Amount</th>
                                <th>Net ChargeAmount</th>
                                <th>Discount </th>


                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            //get rows query
                            $query = $db->query("SELECT  Service_Type, Ground_Service, TRUNCATE(sum(Net_Charge_Amount),2) as Net_Charge_Amount,  TRUNCATE(sum(Transportation_Charge_Amount),2) as Transportation_Charge_Amount, TRUNCATE(sum(Transportation_Charge_Amount - Net_Charge_Amount ),2)  as Discount
FROM imported_data
GROUP BY Service_Type, Ground_Service;");

                            if($query->num_rows > 0){
                                while($row = $query->fetch_assoc()){
                                    ?>
                                    <tr>
                                        <td><?php echo $row['Service_Type']; ?></td>
                                        <td><?php echo $row['Ground_Service'];  ?></td>

                                        <td><?php echo $row['Transportation_Charge_Amount']; ?></td>
                                        <td><?php echo $row['Net_Charge_Amount']; ?></td>
                                        <td><?php echo $row['Discount'] ;?></td>

                                    </tr>
                                <?php } }else{ ?>
                                <tr><td colspan="7">No Data found.....</td></tr>
                            <?php } ?>
                            </tbody>
                        </table>


                        <h3 class="head text-center">Raw Data</h3>


                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Original Amount Due</th>
                                <th>Transportation Charge Amount</th>
                                <th>Net Charge Amount</th>
                                <th>Service Type</th>
                                <th>Ground Service</th>
                                <th>Rated Weight Amount</th>
                                <th># Pieces</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            //get rows query
                            $query = $db->query("SELECT * FROM `imported_data`");
                            if($query->num_rows > 0){
                                while($row = $query->fetch_assoc()){
                                    ?>
                                    <tr>
                                        <td><?php echo $row['Original_Amount_Due']; ?></td>
                                        <td><?php echo $row['Transportation_Charge_Amount']; ?></td>
                                        <td><?php echo $row['Net_Charge_Amount']; ?></td>
                                        <td><?php echo $row['Service_Type']; ?></td>
                                        <td><?php echo $row['Ground_Service']; ?></td>
                                        <td><?php echo $row['Rated_Weight_Amount']; ?></td>
                                        <td><?php echo $row['Number_of_Pieces ']; ?></td>
                                    </tr>
                                <?php } }else{ ?>
                                <tr><td colspan="7">No Data found.....</td></tr>
                            <?php } ?>
                            </tbody>
                        </table>








                        <p class="text-center">
                            <a href="" class="btn btn-success btn-outline-rounded green"> Compare rates <span style="margin-left:10px;" class="glyphicon glyphicon-send"></span></a>
                        </p>
                    </div>
                    <div class="tab-pane fade" id="settings">
                        <h3 class="head text-center">Build Offering</h3>
                        <div id="placeholder"></div>

                            <script id="flickrTemplate" type="text/x-jquery-tmpl">
                                <h4>${ serviceType }</h4>
                                <h5>${ groundService }</h5>
                                <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="">Description</th>
                                    <th class="text-center">Total Charge Amount</th>
                                    <th class="text-center">Zones<br />1-5</th>
                                    <th class="text-center">Zones<br />6-10</th>
                                    <th class="text-center">Zones<br />11-16</th>
                                    <th class="text-center">Null<br />Zones</th>
                                    <th class="text-center">Zones<br />A-Z</th>
                                </tr>
                                </thead>
                                <tbody>
                                {{each(category) categories}}
                                <tr>
                                    <td>${ Description }</td>
                                    <td class="text-right">$${ TotalChargeAmount }</td>
                                    <td class="text-right">$${ Zone1to5 }</td>
                                    <td class="text-right">$${ Zone6to10 }</td>
                                    <td class="text-right">$${ Zone11to16 }</td>
                                    <td class="text-right">$${ ZoneIsAlpha }</td>
                                    <td class="text-right">$${ ZoneIsNull }</td>
                                </tr>
                                {{/each}}
                                </tbody>
                                </table>

                            </script>











                            <!-- <p class="text-center">
                      <a href="" class="btn btn-success btn-outline-rounded green"> Done! Time to Export <span style="margin-left:10px;" class="glyphicon glyphicon-send"></span></a>
                  </p> -->
                    </div>
                    <div class="tab-pane fade" id="doner">
                        <div class="text-center">
                            <i class="img-intro icon-checkmark-circle"></i>
                        </div>
                        <h3 class="head text-center">Save and Download Data</h3>



                        <div class="col-md-8 col-md-offset-3">


                            <form class="form-inline">
                                <label class="sr-only" for="inlineFormInput">Name File</label>
                                <input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" id="inlineFormInput" placeholder="Company Report">

                                <button type="submit" class="btn btn-primary">Save</button>
                                <button type="submit" class="btn btn-secondary">Download Report CSV</button>
                            </form>



                            <br>


                        </div>



                        <!-- <p class="text-center">
                                            <a href="" class="btn btn-success btn-outline-rounded green"> Download FEDEX Data CSV <span style="margin-left:10px;" class="glyphicon glyphicon-send"></span></a>
                        </div>
                         -->

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        $.get("api.php", function(data, status){
            console.log(data);
            $("#flickrTemplate").tmpl(data).appendTo("#placeholder");
        });
    });
</script>
</html>